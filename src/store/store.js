import { createStore } from 'redux'
import rootReducer from '../reducers/reducers'
import Tools from '../components/SketchField/tools'

let initialState = {
    editorSettings: {
        lineWidth: 10,
        lineColor: 'black',
        fillColor: '#68CCCA',
        backgroundColor: 'transparent',
        shadowWidth: 0,
        shadowOffset: 0,
        tool: Tools.Pencil,
        enableRemoveSelected: false,
        fillWithColor: false,
        fillWithBackgroundColor: false,
        drawings: [],
        canUndo: false,
        canRedo: false,
        controlledSize: false,
        sketchWidth: 600,
        sketchHeight: 600,
        stretched: true,
        stretchedX: false,
        stretchedY: false,
        originX: 'left',
        originY: 'top',
        imageUrl:
            'https://files.gamebanana.com/img/ico/sprays/4ea2f4dad8d6f.png',
        expandTools: false,
        expandControls: false,
        expandColors: false,
        expandBack: false,
        expandImages: false,
        expandControlled: false,
        text: 'a text, cool!',
        enableCopyPaste: false
    },
    dataURL: '',
    jsonData: ''
}
// Create a store with all the reducers, the initial state
// and add Redux Devtools compatibility
export default createStore(
    rootReducer,
    initialState,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
)
