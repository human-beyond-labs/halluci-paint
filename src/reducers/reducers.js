import { SAVE_DATAURL, HALLUCINATE_IMAGE } from '../actions/actions'

const initialState = {}

function rootReducer(state = initialState, action) {
    switch (action.type) {
        case SAVE_DATAURL:
            return {
                dataURL: action.dataUrl
            }

        case HALLUCINATE_IMAGE:
            return state

        default:
            return state
    }
}

export default rootReducer
