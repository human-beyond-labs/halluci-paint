import { red } from '@material-ui/core/colors'
import { createMuiTheme } from '@material-ui/core/styles'

// A custom theme for this app
const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#ad0090'
    },
    secondary: {
      main: '#0731a8'
    },
    error: {
      main: red.A400
    },
    background: {
      default: '#e6e6e6'
    }
  }
})

export default theme
