import React from 'react'
import clsx from 'clsx'
import { makeStyles } from '@material-ui/core/styles'
// import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography'
import Button from '@material-ui/core/Button'
import Container from '@material-ui/core/Container'
import Grid from '@material-ui/core/Grid'
import Paper from '@material-ui/core/Paper'
import Link from '@material-ui/core/Link'

import { SketchField } from '../SketchField'

function Copyright() {
    return (
        <Typography variant="body2" color="textSecondary" align="center">
            {'Copyright © '}
            <Link color="inherit" href="https://humanbeyondabs.com/">
                Human Beyond Labs
            </Link>{' '}
            {new Date().getFullYear()}
            {'.'}
        </Typography>
    )
}

const useStyles = makeStyles(theme => ({
    content: {
        flexGrow: 1,
        height: '100vh',
        overflow: 'auto',
        paddingTop: '64px'
    },
    container: {
        paddingTop: theme.spacing(4),
        paddingBottom: theme.spacing(4)
    },
    paper: {
        padding: theme.spacing(2),
        display: 'flex',
        overflow: 'auto',
        flexDirection: 'column'
    },
    fixedHeight: {
        height: 240
    },
    squareImage: {
        height: 512,
        width: 512,
        padding: 0
    }
}))

export default function Main() {
    const classes = useStyles()

    const fixedHeightPaper = clsx(classes.paper, classes.squareImage)

    return (
        <div className={classes.root}>
            <main className={classes.content}>
                <div className={classes.appBarSpacer} />
                <Container maxWidth="lg" className={classes.container}>
                    <Grid container spacing={3}>
                        {/* Drawing Area */}
                        <Grid item xs={12} md={6} lg={6}>
                            <Paper elevation={3} className={fixedHeightPaper}>
                                <SketchField />
                            </Paper>
                        </Grid>
                        {/* Hallucinated Image */}
                        <Grid item xs={12} md={6} lg={6}>
                            <Paper elevation={3} className={fixedHeightPaper}>
                                Hallucinated image
                            </Paper>
                        </Grid>
                        <Grid item xs={12}>
                            <Paper className={classes.paper}>
                                <Button>Hallucinate!</Button>
                            </Paper>
                        </Grid>
                    </Grid>
                </Container>
                <Copyright />
            </main>
        </div>
    )
}
