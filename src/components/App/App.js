import React from 'react'
// import logo from '../../assets/images/logo.svg';
// import './App.css';
import { makeStyles } from '@material-ui/core/styles'
import Main from '../Main'
import Header from '../Header'

const useStyles = makeStyles({
    root: {
        display: 'flex'
    }
})

export default function App() {
    const classes = useStyles()
    return (
        <div className={classes.root}>
            <Header />
            <Main />
        </div>
    )
}
