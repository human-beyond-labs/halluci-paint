export const SAVE_DATAURL = 'SAVE_DATAURL'
export const HALLUCINATE_IMAGE = 'HALLUCINATE_IMAGE'

export function saveDataUrl(dataUrl) {
    return { type: SAVE_DATAURL, dataUrl: dataUrl }
}

export function hallucinateImage() {
    return { type: HALLUCINATE_IMAGE }
}
